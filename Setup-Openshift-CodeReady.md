## Installasi Openshift CodeReady

### Pra instalasi
1. update dan install package dependensi. instalasi di lakukan di server adw2 user codeready.

    ```text
    ssh -l codeready adw2
    sudo dnf update -y; sudo dnf upgrade -y
    sudo dnf install -y NetworkManager wget git httpd-tools
    ```

2. unduh crc package.

    ```text
    wget https://developers.redhat.com/content-gateway/rest/mirror/pub/openshift-v4/clients/crc/latest/crc-linux-amd64.tar.xz
    tar xfv crc-linux-amd64.tar.xz

    sudo mv crc-linux-*-amd64/crc /usr/local/bin

    # verifikasi
    crc version
    ```

3. unduh pull-secret.txt.

    masuk ke laman berikut untuk unduh pull-secret.txt : [`https://cloud.redhat.com/openshift/create/local`](https://cloud.redhat.com/openshift/create/local).

### intallasi
1. jalankan `crc setup` dan `crc start`.

    ```text
    crc setup
   
    # config penggunaan ram, atur sesuai ram yg tersedia, menggunakan satuan MiB
    crc config set memory 40960 ## sekitar 40G
    crc config get memory

    # config enable monitoring 
    crc config set enable-cluster-monitoring true
    crc config get enable-cluster-monitoring

    # secara default penggunaan cpus crc config adalah 4, jalankan command :
    # crc config set set cpus X # X : jumlah cpus yang ingin dialokasikan

    # crc start -p (lokasi pull-secret.txt)
    crc start -p pull-secret.txt
    ```

2. akses codeready.

    jalankan `crc console --credentials` untuk melihat kredensial user
    
    a. akses lewat cli 

    ```text
    crc console --credentials
    eval $(crc oc-env)
    oc login -u (user) -p (password)
    ```
    ![gambar hasil](./uploads/result-install-crc.png)
    
   b. akses lewat web console

   lakukan tunneling ke server yang terinsttall openshift
   ```text
   ssh -l codeready 10.2.2.12 -D6803
   ```
   
   masuk ke link berikut melalui firefox: `https://console-openshift-console.apps-crc.testing/`, pastikan sudah menkonfigurasi network settings di firefox.

   * klik `Advanced...`, kemudian klik `Accept the Risk and Continue`.
   ![gambar ke-1](./uploads/SS-web-console-1.png)
   * masukan username dan password yang didapatkan dari menjalankan perintah `crc console --credentials`.
   ![gambar ke-2](./uploads/SS-web-console-2.png)
   * jika berhasil masuk akan tampil sebagai berikut:
   ![gambar ke-3](./uploads/SS-web-console-3.png)

### pasca installasi
1. Buat user 

   ```text
   # buat file yang akan menyimpan username dan password yang akan di generate menggunakan htpasswd
   # touch (file yg menyimpan username dan password)
   # htpasswd -Bb (nama file) (username) (password)
   touch htpasswd
   htpasswd -Bb htpasswd tester @Adiwerna24
   htpasswd -Bb htpasswd instruktur @Adiwerna24
   htpasswd -Bb htpasswd admin @Adiwerna24
   
   cat htpasswd
   ```

   ![gambar hasil](./uploads/SS-Htpasswd-create-user.png)

2. upload file htpasswd ke web console openshift untuk membuat user openshift.
   Masuk ke web console sebagai kubeadmin mode administrator, lihat menu samping kiri, Pilih `Administration`, kemudian pilih `Cluster Settings`, lihat 3 menu utama di page `Cluster Settings`, pilih `Configuration`, di kolom pencarian ketikkan `Oauth`, Pilih `Oauth`,
   ![gambar ke-1](./uploads/SS-create-user-1.png)
   Muncul page menu `(OA) cluster`, gulirkan cari `Identity providers`, pilih option `Add`, muncul dropdown menu, pilih `HTPasswd`,
   ![gambar ke-2](./uploads/SS-create-user-2.png)
   Masukan Name dan HTPasswd file yang sudah di generate di point 1. Buat user, jika sudah klik `Add`.
   ![gambar ke-3](./uploads/SS-create-user-3.png)

3. Buat group admin dan rolebindings cluster-admin.
   Pilih ke menu User Management, pilih Groups, kemudian pilih Create Group,
   ![gambar ke-1](./uploads/SS-create group-1.png)
   muncul tampilan editor berbasis yaml, ketikan sesuai kebutuhan, jika sudah klik Create
   ```yaml
   apiVersion: user.openshift.io/v1
   kind: Group
   metadata:
     name: admin ## nama groups
   users:
     - tester   ## user-user yang akan masukkan sebagai bagian groups
     - instruktur
     - admin
   ```
   ![gambar ke-2](./uploads/SS-create-group-2.png)  

   masuk ke menu `RoleBindings` dari pilihan menu `User Management`, klik `Create binding`,
   ![gambar ke-3](./uploads/SS-role-binding-groups-1.png)
   muncul page `Create RoleBinding`, pilih `Binding type` ke `Cluster-wide role binding (ClusterRoleBinding)`,
   isikan `Name*`, untuk `Role name*` pilih **`cluster-admin`** **(untuk akun siswa pilih `self-provisioner`)**, di Subject pilih `Group`, isikan `Subject name*` dengan nama group yang sebelumnya dibuat.<br>
   ![gambar ke-4](./uploads/SS-role-binding-groups-2.png)

4. Testing masuk ke user admin
   
   a. masuk melalui cli
   ```text
   oc logout
   oc login -u admin -p (password)
   ```
   ![gambar ke-1](./uploads/test-login-1.png)

   b. masuk melalui web console
   
   log out dari web console user kubeadmin
   ![gambar ke-2](./uploads/test-login-2.png)
   pilih login option yang kita buat di point 2 pasca installasi (instruktur)
   ![gambar ke-3](./uploads/test-login-3.png)
   masukkan username dan passwordnya
   ![gambar ke-4](./uploads/test-login-4.png)
   jika berhasil akan tampil sebagai berikut.
   ![gambar ke-5](./uploads/test-login-5.png)

5. Tambah direktori di vm crc untuk modul 7 lab 7.2 dan quiz-6.

   remote crc vm memakai user core dengan id rsa key di `~/.crc/machines/crc/id_ecdsa`. untuk ip di dapatkan dari command `sudo virsh net-dhcp-leases crc`, ip defaultnya `192.168.130.11`.
   ```text
   ssh -i ~/.crc/machines/crc/id_ecdsa -l core 192.168.130.11
   mkdir -p mysql/{data,init}
   sudo semanage fcontext -a -t container_file_t '/var/home/core/mysql(/.*)?'
   sudo restorecon -R /var/home/core/mysql
   sudo chown -R 27:27 mysql/
   ls -lZd mysql/ ##verifikasi
   
   exit # keluar vm
   ```

6. Konfigurasi iptables agar openshift crc dapat di akses dari luar server atau dari vm diluar network vm crc.<br>
   dari user root jalankan command berikut.
   ```text
   # iptables -t nat -L PREROUTING -d (ip server) -p tcp --dport (port yg digunakan) -j DNAT --to-destination (ip vm crc):(port yang digunakan)
   iptables -t nat -A PREROUTING -d 10.2.2.12 -p tcp --dport 6443 -j DNAT --to-destination 192.168.130.11:6443
   iptables -t nat -A PREROUTING -d 10.2.2.12 -p tcp --dport 443 -j DNAT --to-destination 192.168.130.11:443
   iptables -t nat -A PREROUTING -d 10.2.2.12 -p tcp --dport 80 -j DNAT --to-destination 192.168.130.11:80
   ```
   flush chain LIBVIRT_FWI dan iptables -F LIBVIRT_FWO
   ```text
   iptables -F LIBVIRT_FWI
   iptables -F LIBVIRT_FWO
   ```
   
7. konfigurasi untuk vm yang akan mengakses openshift<br>
   > **Note: Jalankan intruksi di bawah untuk vm yang digunakan sebagai oc client**<br>

   konfigurasi yang perlu ditambahkan di /etc/hosts 
   ```text
   10.2.2.12 api.crc.testing canary-openshift-ingress-canary.apps-crc.testing console-openshift-console.apps-crc.testing default-route-openshift-image-registry.apps-crc.testing downloads-openshift-console.apps-crc.testing oauth-openshift.apps-crc.testing
   ```
   konfigurasi dnsmasq agar tidak perlu konfigurasi /etc/hosts setiap kali membuat route service app openshift untuk mengaksesnya
   ```text
   sudo dnf install dnsmasq -y
   sudo vim /etc/dnsmasq.conf
   ...
   address=/apps-crc.testing/10.2.2.12
   address=/api.crc.testing/10.2.2.12
   ...

   sudo vim /etc/resolv.conf
   ...
   search localdomain
   nameserver 127.0.0.1  ## pastikan 'nameserver 127.0.0.1' diatas dns 'nameserver 8.8.8.8'
   nameserver 8.8.8.8
   ...

   sudo systemctl enable --now dnsmasq
   ```

   install oc client
   
   cari link instalasi, masuk ke dashboard openshift.
   ![gambar ke-1](./uploads/install-oc-1.png)
   ![gambar ke-2](./uploads/install-oc-2.png)

   unduh oc client package menggunakan wget.
   ```text
   wget --no-check-certificate https://downloads-openshift-console.apps-crc.testing/amd64/linux/oc.tar
   tar xvf oc.tar
   sudo cp oc /usr/local/bin/oc
   sudo chmod +x /usr/local/bin/oc
   oc version
   ````